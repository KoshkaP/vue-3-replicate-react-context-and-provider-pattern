# Vue 3 -- React Context and Provider Pattern 

code example on how to use Vue's **provide / inject**

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
[Configuration Reference](https://cli.vuejs.org/config/).

### Documentation
- [provide/inject](https://v3.vuejs.org/guide/component-provide-inject.html#provide-inject)
- [Vue Loader (CSS Modules)](https://vue-loader.vuejs.org/guide/css-modules.html)
